package com.werum.schulung.ttt.application;

import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Diese Klasse stellt die Applikation fuer das TicTacToe Spiel da. Das Design
 * ist ueber den Scenebuilder zu erstellen, die Infrastruktur dafuer ist bereits
 * vorhanden.
 * 
 * @author jasmin_sander
 */
public class TicTacToeApplication extends Application {

	TicTacToeController tictactoeController;

	@Override
	public void start(Stage primaryStage) throws IOException {

		// FXML Datei laden
		File fxmlFile = new File("src/com/werum/schulung/ttt/application/tictactoe.fxml");
		FXMLLoader fxml = new FXMLLoader(fxmlFile.toURI().toURL());
		Parent root = fxml.load();

		// Controller-Klasse festlegen
		tictactoeController = fxml.getController();
		tictactoeController.setMainApp(this);

		int width = 0; // TODO Waehle groe�e des Fensters
		int heigth = 0; // (z.B. Uebernehmen der Groe�e des Containers)

		// Scene erstellen
		Scene scene = new Scene(root, width, heigth);

		// Stage konfiguieren
		primaryStage.setTitle("TicTacToe");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
